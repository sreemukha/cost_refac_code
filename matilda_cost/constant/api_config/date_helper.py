""" 8/07/2018, 05:17 PM """
# coding=utf-8
# !/usr/bin/env python

from matilda_cost.mock_data.amazon.mock_data_helper import MockDataHelper
from matilda_cost.constant.matilda_enum import FormatterConstants
from matilda_cost.cloud_providers.amazon.cost_estimation import GenericHelper
import logging as log
import datetime

# "TimePeriod": {
#     "Start": "yyyy-mm-dd",
#     "End": "yyyy-mm-dd"
# }

class DateHelper(object):

    def __init__(self):
        """Date helper"""
        self.current_date  = datetime.datetime.now().strftime("%Y-%m-%d")
        self.current_month_start_date = datetime.datetime.now().replace(day=1).strftime("%Y-%m-%d")

    def get_current_date(self):
        return self.current_date

    def get_current_month_start_date(self):
        return self.current_month_start_date





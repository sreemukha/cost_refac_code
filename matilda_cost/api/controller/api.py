<<<<<<< HEAD
import json, os, jsonify
from oslo_log import log
from flask import Flask, jsonify, request, render_template
from flask_cors import CORS
from flask_oslolog import OsloLog
from matilda_cost.api.controller import api_handler


app = Flask(__name__)
CORS(app)
LOG = OsloLog(app).logger


def prefix_route(route_function, prefix='', mask='{0}{1}'):
    '''
      Defines a new route function with a prefix.
      The mask argument is a `format string` formatted with, in that order:
        prefix, route
    '''
    def newroute(route, *args, **kwargs):
        '''New function to prefix the route'''
        return route_function(mask.format(prefix, route), *args, **kwargs)
    return newroute

#app.route = prefix_route(app.route, '/matilda/cost')

@app.route('/aws_ce', methods=['GET', 'POST'])
def post_json_handler():
    content = json.loads(request.data)
    account_name = request.args.get('account_name')
    resp = api_handler.cost_estimation(req_data=content, account_name=account_name)
    return jsonify(resp)


@app.route('/aws_ce/granularity', methods=['GET'])
def get_granularity():
    resp = api_handler.get_granularity()
    return jsonify(resp)


@app.route('/aws_ce/metrics', methods=['GET'])
def get_metrics():
    resp = api_handler.get_metrics()
    return jsonify(resp)


@app.route('/aws_ce/groupby', methods=['GET'])
def get_group_by():
    # sample http://192.168.20.156:8090/aws_ce/groupby?start=2018-07-10&end=2018-07-17
    start_date = request.args.get('start')
    end_date = request.args.get('end')
    account_name = request.args.get('account_name')
    resp = api_handler.get_group_by(start_date, end_date, account_name)
    return jsonify(resp)


@app.route('/aws_ce/filter', methods=['GET'])
def get_filter():
    # http://192.168.20.156:8090/aws_ce/filter?start=2018-07-10&end=2018-07-17
    start_date = request.args.get('start')
    end_date = request.args.get('end')
    account_name = request.args.get('account_name')
    resp = api_handler.get_filter(start_date, end_date, account_name)
    return jsonify(resp)

@app.route('/', methods=['GET'])
def matilda_cost():
    return 'Welcome to MATILDA COST MANAGEMENT COMPONENT'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6010, debug=True)
=======
import json, os, jsonify
from oslo_log import log
from flask import Flask, jsonify, request, render_template
from flask_cors import CORS
from flask_oslolog import OsloLog
from matilda_cost.api.controller import api_handler


app = Flask(__name__)
CORS(app)
LOG = OsloLog(app).logger


def prefix_route(route_function, prefix='', mask='{0}{1}'):
    '''
      Defines a new route function with a prefix.
      The mask argument is a `format string` formatted with, in that order:
        prefix, route
    '''
    def newroute(route, *args, **kwargs):
        '''New function to prefix the route'''
        return route_function(mask.format(prefix, route), *args, **kwargs)
    return newroute

app.route = prefix_route(app.route, '/matilda/cost')

@app.route('/aws_ce', methods=['GET', 'POST'])
def post_json_handler():
    content = json.loads(request.data.decode('utf-8'))
    account_name = request.args.get('account_name')
    resp = api_handler.cost_estimation(req_data=content, account_name=account_name)
    return jsonify(resp)

@app.route('/aws_ce/monthly_cost', methods=['GET'])
def get_monthly_cost():
    # sample http://192.168.20.156:6010/aws_ce/monthly_cost?account_name=matilda-admin
    account_name = request.args.get('account_name')
    resp = api_handler.get_monthly_cost(account_name=account_name)
    return jsonify(resp)

@app.route('/aws_ce/monthly_cost_services', methods=['GET'])
def get_monthly_cost_services():
    # sample http://192.168.20.156:6010/aws_ce/monthly_cost_services?account_name=matilda-admin
    account_name = request.args.get('account_name')
    resp = api_handler.get_monthly_cost_services(account_name=account_name)
    return jsonify(resp)

@app.route('/aws_ce/monthly_cost/tag/<tag_name>', methods=['GET'])
def get_monthly_cost_tags(tag_name):
    # sample http://192.168.20.156:6010/aws_ce/monthly_cost/tag/_services?account_name=matilda-admin
    account_name = request.args.get('account_name')
    resp = api_handler.get_monthly_cost_tags(account_name=account_name, tag_name=tag_name)
    return jsonify(resp)

@app.route('/aws_ce/granularity', methods=['GET'])
def get_granularity():
    # http://192.168.20.156:6010/matilda/cost/aws_ce/granularity
    resp = api_handler.get_granularity()
    return jsonify(resp)


@app.route('/aws_ce/metrics', methods=['GET'])
def get_metrics():
    # sample http://192.168.20.156:6010/matilda/cost/aws_ce/metrics
    resp = api_handler.get_metrics()
    return jsonify(resp)


@app.route('/aws_ce/groupby', methods=['GET'])
def get_group_by():
    # sample  http://192.168.20.156:6010/matilda/cost/aws_ce/groupby?start=2018-08-10&end=2018-08-17&account_name=matilda-admin
    start_date = request.args.get('start')
    end_date = request.args.get('end')
    account_name = request.args.get('account_name')
    resp = api_handler.get_group_by(start_date, end_date, account_name)
    return jsonify(resp)


@app.route('/aws_ce/filter', methods=['GET'])
def get_filter():
    # sample http://192.168.20.156:6010/aws_ce/filter/tag?start=2018-10-01&end=2018-10-10&account_name=matilda-admin
    start_date = request.args.get('start')
    end_date = request.args.get('end')
    account_name = request.args.get('account_name')
    resp = api_handler.get_filter(start_date, end_date, account_name)
    return jsonify(resp)

@app.route('/aws_ce/filter/tag', methods=['GET'])
def get_filter_application():
    # sample http://192.168.20.156:6010/aws_ce/filter/tag?start=2018-10-01&end=2018-10-10&account_name=matilda-admin&tag_name=Application
    start_date = request.args.get('start')
    end_date = request.args.get('end')
    account_name = request.args.get('account_name')
    tag_name = request.args.get('tag_name')
    resp = api_handler.get_filter_for_tag_name(start_date, end_date, account_name, tag_name)
    return jsonify(resp)

@app.route('/', methods=['GET'])
def matilda_cost():
    return 'Welcome to MATILDA COST MANAGEMENT COMPONENT'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6010, debug=True)
>>>>>>> 6e051419d458848a4b35bcbd65d4a958930e7c48
